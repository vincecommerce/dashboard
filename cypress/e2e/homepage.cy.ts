describe('<Homepage />', () => {
  it('page titles check', () => {
    cy.visit('/')
    cy.contains('[data-cy="page-title"]', 'Investment')
    cy.contains('[data-cy="stock-title"]', 'Find stock:')
  })
})
