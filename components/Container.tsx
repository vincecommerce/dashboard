const Container = ({ children }: { children: React.ReactNode }) => {
  return <div className="mx-8">{children}</div>
}

export default Container
