import {
  alphaVantageKey,
  fastForexApiKey,
  marketStackKey,
} from '../utils/config'

interface ICurrencyRates {
  results: {
    [key: string]: number
  }
}

export interface IStocksData {
  [key: string]: string
}

interface IStocks {
  bestMatches: IStocksData[]
}

interface IEod {
  open: number
}

export interface IStock {
  Name: string
  Symbol: string
  Exchange: string
  Type: string
  Country: string
  value: number
  Industry: string
  Sector: string
}

export const getCurrencyRates = async (): Promise<ICurrencyRates> => {
  const res = await fetch(
    `/api/fastforex/fetch-multi?api_key=${fastForexApiKey}&from=USD&to=EUR,GBP`,
    {
      cache: 'force-cache',
    }
  )

  if (!res.ok) {
    throw new Error('Failed to fetch rates')
  }

  return res.json() as unknown as ICurrencyRates
}

export const getStocksByName = async (name: string): Promise<IStocksData[]> => {
  if (name === '') return []

  const res = await fetch(
    `/api/alphavantage/query?function=SYMBOL_SEARCH&keywords=${name}&apikey=${alphaVantageKey}&limit=10`
  )

  if (!res.ok) {
    throw new Error('Failed to fetch stocks')
  }

  const tickers = (await res.json()) as unknown as IStocks

  return tickers.bestMatches
}

export const getStock = async (item: IStocksData): Promise<IStock> => {
  const symbol: string = item['1. symbol']
  const name: string = item['2. name']
  const type: string = item['3. type']

  const resCompany = await fetch(
    `/api/alphavantage/query?function=OVERVIEW&symbol=${symbol}&apikey=${alphaVantageKey}`
  )

  if (!resCompany.ok) {
    throw new Error('Failed to fetch company infos')
  }

  const company = (await resCompany.json()) as unknown as IStock

  const resEod = await fetch(
    `/api/marketstack/tickers/${symbol}/eod/latest?access_key=${marketStackKey}`
  )

  if (!resEod.ok) {
    if (resEod.status === 404) {
      company.value = 0
    } else {
      throw new Error('Failed to fetch eod infos')
    }
  }

  const eod = (await resEod.json()) as unknown as IEod

  company.Symbol = company.Symbol ?? symbol
  company.Name = company.Name ?? name
  company.Type = company.Type ?? type
  company.value = company.value ?? eod.open

  return company
}
