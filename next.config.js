/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: false,
  distDir: 'build',
  env: {
    fastForexKey: '9c99c8ee73-dc6129daec-ri58a7',
    marketStackKey: '0480b7602a14e8d577eaf92191a930a1',
    alphaVantageKey: 'E17TNSCKO51IAO9H',
  },
  async rewrites() {
    return [
      {
        source: '/api/fastforex/:path*',
        destination: 'https://api.fastforex.io/:path*',
      },
      {
        source: '/api/marketstack/:path*',
        destination: 'https://api.marketstack.com/v1/:path*',
      },
      {
        source: '/api/alphavantage/:path*',
        destination: 'https://www.alphavantage.co/:path*',
      },
    ]
  },
}

module.exports = nextConfig
