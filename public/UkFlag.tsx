const UkFlag = () => {
  return (
    <svg
      width="18"
      height="18"
      viewBox="0 0 20 16"
      fill="none"
      xmlns="http://www.w3.org/2000/svg">
      <rect
        y="0.856934"
        width="20"
        height="14.2857"
        rx="1.71429"
        fill="white"
      />
      <mask
        id="mask0_134_4567"
        className="mask-type:luminance"
        maskUnits="userSpaceOnUse"
        x="0"
        y="0"
        width="20"
        height="16">
        <rect
          y="0.856934"
          width="20"
          height="14.2857"
          rx="1.71429"
          fill="white"
        />
      </mask>
      <g mask="url(#mask0_134_4567)">
        <rect y="0.856934" width="20" height="14.2857" fill="#0A17A7" />
        <path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M-0.91628 -0.511719L7.6188 5.24527V-0.0952097H12.3807V5.24526L20.9158 -0.511717L21.9809 1.0674L15.2328 5.61908H19.9998V10.381H15.2328L21.9809 14.9327L20.9158 16.5118L12.3807 10.7548V16.0953H7.6188V10.7548L-0.916316 16.5118L-1.98145 14.9327L4.76669 10.381H-0.000244141V5.61908H4.76672L-1.98141 1.0674L-0.91628 -0.511719Z"
          fill="white"
        />
        <path
          d="M13.3345 5.37979L22.3812 -0.571777"
          stroke="#DB1F35"
          strokeWidth="0.571429"
          strokeLinecap="round"
        />
        <path
          d="M14.2949 10.6411L22.4048 16.1074"
          stroke="#DB1F35"
          strokeWidth="0.571429"
          strokeLinecap="round"
        />
        <path
          d="M5.71809 5.36461L-2.74121 -0.336426"
          stroke="#DB1F35"
          strokeWidth="0.571429"
          strokeLinecap="round"
        />
        <path
          d="M6.63559 10.5747L-2.74121 16.793"
          stroke="#DB1F35"
          strokeWidth="0.571429"
          strokeLinecap="round"
        />
        <path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M0 9.42836H8.57143V15.1426H11.4286V9.42836H20V6.57122H11.4286V0.856934H8.57143V6.57122H0V9.42836Z"
          fill="#E6273E"
        />
      </g>
    </svg>
  )
}

export default UkFlag
