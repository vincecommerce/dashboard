const InvestmentIcon = () => {
  return (
    <svg
      width="20"
      height="21"
      viewBox="0 0 20 21"
      fill="none"
      xmlns="http://www.w3.org/2000/svg">
      <path
        d="M7.5871 9.71191H4.21401C3.62227 9.71191 3.14258 10.1916 3.14258 10.7833V15.2784C3.14258 15.8701 3.62227 16.3498 4.21401 16.3498H7.5871"
        stroke="url(#paint0_linear_104_11019)"
        strokeWidth="1.42857"
      />
      <path
        d="M7.5874 7.46303C7.5874 6.8713 8.0671 6.3916 8.65883 6.3916H12.0319V11.37V16.3485H7.5874V7.46303Z"
        stroke="url(#paint1_linear_104_11019)"
        strokeWidth="1.42857"
      />
      <path
        d="M12.0317 4.14272C12.0317 3.55098 12.5114 3.07129 13.1032 3.07129H15.4048C15.9966 3.07129 16.4763 3.55098 16.4763 4.14272V9.70921V15.2757C16.4763 15.8674 15.9966 16.3471 15.4048 16.3471H12.0317V4.14272Z"
        stroke="url(#paint2_linear_104_11019)"
        strokeWidth="1.42857"
      />
      <defs>
        <linearGradient
          id="paint0_linear_104_11019"
          x1="0.285385"
          y1="10.0438"
          x2="9.14005"
          y2="17.6932"
          gradientUnits="userSpaceOnUse">
          <stop stopColor="#17D2C3" />
          <stop offset="0.716539" stopColor="#4579F5" />
          <stop offset="1" stopColor="#8550F5" />
        </linearGradient>
        <linearGradient
          id="paint1_linear_104_11019"
          x1="7.52893"
          y1="-1.60493"
          x2="20.4414"
          y2="7.58031"
          gradientUnits="userSpaceOnUse">
          <stop stopColor="#8550F5" />
          <stop offset="0.456772" stopColor="#4579F5" />
          <stop offset="1" stopColor="#17D2C3" />
        </linearGradient>
        <linearGradient
          id="paint2_linear_104_11019"
          x1="12.3492"
          y1="1.7437"
          x2="22.7856"
          y2="6.50749"
          gradientUnits="userSpaceOnUse">
          <stop stopColor="#17D2C3" />
          <stop offset="0.640573" stopColor="#4579F5" />
          <stop offset="1" stopColor="#8550F5" />
        </linearGradient>
      </defs>
    </svg>
  )
}

export default InvestmentIcon
