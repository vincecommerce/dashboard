const RefreshIcon = (props: any) => {
  return (
    <svg
      {...props}
      width="20"
      height="20"
      viewBox="0 0 20 20"
      fill="none"
      xmlns="http://www.w3.org/2000/svg">
      <path
        d="M3.5 11.5789C3.5 11.5789 3.58092 12.1153 5.92521 14.3351C8.26951 16.555 12.0704 16.555 14.4146 14.3351C15.2452 13.5486 15.7815 12.589 16.0236 11.5789M3.5 11.5789V15.3684M3.5 11.5789H7.50196M16.5 8.42107C16.5 8.42107 16.4191 7.88471 14.0748 5.66489C11.7305 3.44506 7.92964 3.44506 5.58535 5.66489C4.75476 6.45138 4.21845 7.411 3.97642 8.42107M16.5 8.42107V4.6316M16.5 8.42107H12.498"
        stroke="#3F3F52"
        strokeWidth="1.8"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  )
}

export default RefreshIcon
