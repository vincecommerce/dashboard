'use client'

import { Combobox, Listbox, Transition } from '@headlessui/react'
import { CheckIcon, TrashIcon } from '@heroicons/react/20/solid'
import Image from 'next/image'
import { Fragment, useEffect, useState } from 'react'
import {
  IStock,
  IStocksData,
  getCurrencyRates,
  getStock,
  getStocksByName,
} from '../api/apis'
import Container from '../components/Container'
import BankIcon from '../public/BankIcon'
import InvestmentIcon from '../public/InvestmentIcon'
import RefreshIcon from '../public/RefreshIcon'
import SearchIcon from '../public/SearchIcon'
import { truncateText } from '../utils'
import Flag from '../utils/Flag'
import { ICurrencies, converter, currencies } from '../utils/currencies'
import { headerItems } from '../utils/table-header'

export default function Home() {
  const [currency, setCurrency] = useState<ICurrencies>(currencies[1])
  const [rate, setRate] = useState<number>(0)

  const [selected, setSelected] = useState<IStock[]>([])
  const [stocks, setStocks] = useState<IStocksData[]>([])
  const [query, setQuery] = useState<string>('')
  const [refresh, setRefresh] = useState<boolean>(false)

  const handleSearchClick = async (item: IStocksData) => {
    const stock = await getStock(item)
    setSelected((prev) => [...prev, stock])
  }

  const handleStockRemove = (position: number) => {
    const filtered = selected.filter((item, index) => index !== position)
    setSelected(() => filtered)
  }

  useEffect(() => {
    const main = async () => {
      const rates = await getCurrencyRates()
      setRate(() =>
        currency.name !== 'usd' ? rates.results[currency.name.toUpperCase()] : 1
      )
    }

    const reload = async () => {
      await main()
      setRefresh(() => !refresh)
    }

    refresh ? reload() : main()
  }, [currency, refresh])

  useEffect(() => {
    const main = async () => {
      const data = await getStocksByName(query)
      setStocks(() => data)
    }

    main()
  }, [query])

  return (
    <Container>
      <div className="h-20 flex justify-between items-center border-b-2 border-b-[#D3D3E0]">
        <div className="text-2xl font-semibold" data-cy="page-title">
          Investment
        </div>
        <div className="flex justify-between items-center gap-4">
          <Listbox value={currency} onChange={setCurrency}>
            <div className="relative mt-1">
              <Listbox.Button className="relative w-full cursor-default rounded-[8px] shadow-sm bg-white py-2.5 pl-3 pr-10 text-left focus:outline-none focus-visible:border-indigo-500 focus-visible:ring-2 focus-visible:ring-white focus-visible:ring-opacity-75 focus-visible:ring-offset-2 focus-visible:ring-offset-orange-300 sm:text-sm">
                <span className="flex gap-2.5 font-medium justify-between items-center uppercase">
                  <Flag name={currency.img} />
                  {currency.name}
                </span>
                <span className="pointer-events-none absolute inset-y-0 right-0 flex items-center pr-2">
                  <Image
                    src="/dropdown.png"
                    width={20}
                    height={20}
                    alt={'no icon'}
                  />
                </span>
              </Listbox.Button>
              <Transition
                as={Fragment}
                leave="transition ease-in duration-100"
                leaveFrom="opacity-100"
                leaveTo="opacity-0">
                <Listbox.Options className="absolute mt-1 max-h-60 w-36 py-2 overflow-auto rounded-[8px] bg-white text-base shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none sm:text-sm right-0 z-10">
                  {currencies.map((item, index) => (
                    <Listbox.Option
                      key={index}
                      className={({ active }) =>
                        `relative cursor-pointer select-none px-3 py-2 mx-2 uppercase rounded-[8px] ${
                          active ? 'bg-[#EBEBF3] ' : 'bg-white'
                        }`
                      }
                      value={item}>
                      {({ selected }) => (
                        <div>
                          <span
                            className={`block truncate ${
                              selected ? 'font-medium' : 'font-normal'
                            }`}>
                            <span className="flex items-center gap-2.5">
                              <Flag name={item.img} />
                              {item.name}
                            </span>
                          </span>
                          {selected ? (
                            <span className="absolute inset-y-0 right-0 flex items-center ml-10 pr-3 text-[#5547F6]">
                              <CheckIcon
                                className="h-5 w-5"
                                aria-hidden="true"
                              />
                            </span>
                          ) : null}
                        </div>
                      )}
                    </Listbox.Option>
                  ))}
                </Listbox.Options>
              </Transition>
            </div>
          </Listbox>
          <button
            className="bg-white p-2.5 shadow-sm rounded-[8px]"
            onClick={() => setRefresh(() => !refresh)}>
            <RefreshIcon className={`${refresh ?? 'animate-spin'}`} />
          </button>
        </div>
      </div>
      <div className="flex flex-wrap">
        <div className="flex w-full font-medium mt-3" data-cy="stock-title">
          Find stock:
        </div>
        <div className="w-full">
          <Combobox value={selected}>
            <div className="relative mt-1">
              <div className="relative flex items-center focus-within:border-b-[#5547F6] border-b-2 stroke-[#9494B2] focus-within:stroke-[#5547F6] border-b-[#D3D3E0] w-full h-14">
                <SearchIcon className="text-transparent fill-current h-5 w-5" />
                <Combobox.Input
                  className="w-full mx-4 placeholder:text-[1rem] placeholder:text-[#9494B2] text-sm bg-transparent outline-none leading-5 text-[#1A1C1E] z-0"
                  placeholder="Search by Stocks, Shares, ETFs, or ticket symbol"
                  onChange={(event) => setQuery(event.target.value)}
                />
              </div>
              <Transition
                as={Fragment}
                leave="transition ease-in duration-100"
                leaveFrom="opacity-100"
                leaveTo="opacity-0"
                afterLeave={() => setQuery('')}>
                <Combobox.Options className="absolute mt-1 max-h-60 w-full overflow-auto rounded-[8px] bg-white py-2 text-base shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none sm:text-sm">
                  {stocks.length === 0 && query !== '' ? (
                    <div className="relative cursor-default select-none py-2 px-4 text-gray-700">
                      Nothing found.
                    </div>
                  ) : (
                    stocks.map((stock, index) => (
                      <Combobox.Option
                        key={index}
                        className={({ active }) =>
                          `relative cursor-pointer select-none mx-2 py-[8px] px-[12px] rounded-[8px] ${
                            active
                              ? 'bg-[#EBEBF3] text-[#1C1F22]'
                              : 'text-[#1C1F22]'
                          }`
                        }
                        value={stock}
                        onClick={() => handleSearchClick(stock)}>
                        {() => (
                          <div className="flex items-center gap-6">
                            <span className="flex gap-2">
                              <Image
                                src={`/apple.png`}
                                alt={stock['1. symbol']}
                                width={22}
                                height={22}
                              />
                              {stock['2. name']}
                            </span>
                            <span className="text-[#9494B2] inline-block bg-white rounded-full px-1.5 py-1 text-xs">
                              {stock['1. symbol']}
                            </span>
                            <span className="text-[#9494B2] inline-block bg-white rounded-full px-1.5 py-1 text-xs">
                              {stock['3. type']}
                            </span>
                          </div>
                        )}
                      </Combobox.Option>
                    ))
                  )}
                </Combobox.Options>
              </Transition>
            </div>
          </Combobox>
        </div>
      </div>
      <div
        className={`flex xl:overflow-x-hidden overflow-auto rounded-lg mt-10 ${
          selected.length <= 0 ? 'border-0 justify-center' : 'border-2'
        }`}>
        {selected.length <= 0 ? (
          <div className="cursor-default select-none py-2 px-4 text-gray-700">
            No item selected
          </div>
        ) : (
          <table className="table-auto w-full rounded-lg bg-transparent">
            <thead className="bg-transparent border-b border-b-[#D3D3E0] text-left">
              <tr>
                {headerItems.map((item, index) => (
                  <th
                    className="text-[#77779A] text-sm font-normal px-8 py-6 whitespace-nowrap"
                    key={index}>
                    {item}
                  </th>
                ))}
              </tr>
            </thead>
            <tbody className="bg-white">
              {selected.map((stock, index) => (
                <tr className="border-b text-[#1A1C1E] max-w-xs" key={index}>
                  <td className="border-r px-8 py-3 whitespace-nowrap ">
                    <div className="flex items-center gap-3">
                      <Image
                        src={`/apple.png`}
                        alt={'no image'}
                        width={20}
                        height={20}
                      />
                      {truncateText(stock.Name)}
                    </div>
                  </td>
                  <td className="border-r px-8 py-3 tracking-wide whitespace-nowrap">
                    {stock.Symbol}
                  </td>
                  <td className="border-r px-8 py-3 tracking-wide whitespace-nowrap">
                    {stock.Exchange}
                  </td>
                  <td className="border-r px-8 py-3 tracking-wide whitespace-nowrap">
                    <div className="flex items-center gap-3 capitalize">
                      {stock.Type.toLocaleLowerCase() === 'mutual fund' ? (
                        <BankIcon />
                      ) : (
                        <InvestmentIcon />
                      )}
                      {stock.Type}
                    </div>
                  </td>
                  <td className="border-r px-8 py-3 tracking-wide whitespace-nowrap">
                    <div className="flex items-center gap-3 uppercase">
                      <Flag
                        name={
                          stock.Country ? stock.Country.toLocaleLowerCase() : ''
                        }
                      />
                      {stock.Country}
                    </div>
                  </td>
                  <td className="border-r px-8 py-3 tracking-wide whitespace-nowrap">
                    {stock.value === 0
                      ? 'NaN'
                      : converter(stock.value, rate, currency.symbol)}
                  </td>
                  <td className="truncate border-r px-8 py-3 tracking-wide whitespace-nowrap">
                    {truncateText(stock.Industry)}
                  </td>
                  <td className="px-8 py-3 tracking-wide whitespace-nowrap">
                    {stock.Sector}
                  </td>
                  <td className="py-3 px-8 tracking-wide whitespace-nowrap">
                    <button
                      className="flex items-center"
                      onClick={() => handleStockRemove(index)}>
                      <TrashIcon
                        className="h-5 w-5 text-[#5547F6]"
                        aria-hidden="true"
                      />
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        )}
      </div>
    </Container>
  )
}
