interface IStockItems {
  img: string
  name: string
  symbol: string
  exchange: string
  type: string
  country: string
  value: number
  category: string
}

export const stockItems: IStockItems[] = [
  {
    img: 'apple',
    name: 'Apple',
    symbol: '$AAPL',
    exchange: 'Interactive Broker',
    type: 'mutual-fund',
    country: 'us',
    value: 40,
    category: 'industry',
  },
  {
    img: 'apple',
    name: 'Apple',
    symbol: '$AAPL',
    exchange: 'Interactive Broker',
    type: 'equity',
    country: 'uk',
    value: 34,
    category: 'industry',
  },
]
