export interface ICurrencies {
  img: string
  name: string
  symbol: string
}

export const currencies: ICurrencies[] = [
  {
    img: 'uk',
    name: 'gbp',
    symbol: '£',
  },
  {
    img: 'usa',
    name: 'usd',
    symbol: '$',
  },
  {
    img: 'eur',
    name: 'eur',
    symbol: '€',
  },
]

export const converter = (
  value: number,
  rate: number,
  symbol: string
): string => {
  const amount = value ? value : 0
  return `${symbol}${(amount * rate).toFixed(2)}`
}
