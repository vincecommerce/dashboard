import EuFlag from '../public/EuFlag'
import UkFlag from '../public/UkFlag'
import UsFlag from '../public/UsFlag'

interface FlagProps {
  name: string
}

const Flag = ({ name }: FlagProps) => {
  return name === 'uk' ? (
    <UkFlag />
  ) : name === 'usa' ? (
    <UsFlag />
  ) : name === 'eur' ? (
    <EuFlag />
  ) : (
    <></>
  )
}

export default Flag
